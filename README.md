# Ball game

## Build
To build the app, simply open the project in Android Studio and click the "run APP" button. Unless you use an emulator with sensor simulating
capabilities, you should use an external Android Device for this. 

## Usage
To throw the virtual ball, simply hold your phone and do a throwing motion upwards. You can see the phones current acceleration on the screen.
For sensitivity adjustments(the lowest required acceleration for a throw event to occur), please navigate to the settings activity and slide the bar
to your preference. 
If your throw is higher than the previous best throw, the highscore will be updated and you will hear a "yaay" sound effect. 
If you however fail to throw higher than the current highscore, a "boo" sound effect will play.
You can always see the height of your latest throw. You will get soem haotic feedback everytime the ball leaves your hand and returns to your hand. 
A sound will play on the highest point in the balls trajectory. 

## Code organization
The java code is organized in two Activity classes. No other classes has been used. There are methods for initializing the textviews and mediaplayers.
There is also a method for initiating the "throw" event. There are comments where the code is not necessarily self-explanatory. 

## Bonus features
* Sound effects reflecting how you did compared to previous throws
* Haptic feedback when ball leaves hand and returns to hand
* You can always see how your last throw compares to the best throw

## Missing features
* The app does not show a counter as the ball travels upwards, only shows the actual height of the throw. This is however the way I understood the text in the specification. 