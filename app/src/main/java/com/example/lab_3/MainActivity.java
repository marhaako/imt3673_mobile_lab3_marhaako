package com.example.lab_3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;
    private float best = 10;
    private int windowSize = 5;
    private static int minAcc = 20;
    private static final float GRAVITY = 9.8f;
    private static float readings[];
    private MediaPlayer mp, mp1, mp2;
    boolean ballInAir = false;  //Boolean used as a "lock" for allowing the user to throw or not
    private int counter = 0;
    private TextView text, text1, text2;
    private Button settingsButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMediaPlayers();
        initTextViews();
        initButtons();


        readings = new float[windowSize];


        SensorManager sensorManager;
        Sensor accelerometer;
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);



        SensorEventListener listener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];


                float v0 = (float) Math.sqrt(x * x + y * y + z * z);  //Calculate initial speed
                readings[counter++] = v0; //Add reading to the readings array

                if(counter == windowSize) {
                    float highest = findHighest(readings);
                    if(highest > minAcc && !ballInAir) {
                        ballInAir = true;         //Ball is not in hand
                        highest -= GRAVITY;       //Make the Gravity play a role when throw is initiated
                        startThrow(highest);      //Initiate the throw
                    }
                    text1.setText(String.format(Locale.getDefault(), "%.2f", highest));

                    counter = 0; //Reset the counter to start getting new readings
                }



            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        sensorManager.registerListener(listener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                intent.putExtra("minAcc", minAcc);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                minAcc = data.getIntExtra("minAcc", 20);
            }
        }
    }

    public void startThrow(final float v0) {   //Function for initiating the "throw" event
        final Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(200);


        final float t = v0 / GRAVITY;
        final float h = v0 * t + 0.5f * GRAVITY * t*t;

        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                mp.start();

            }

        }, (int)t*1000);

        (new Handler()).postDelayed(new Runnable() {
            public void run() {

                v.vibrate(200);

                if(v0 > best) {  //Checks if the initial speed is the best.
                    best = v0;
                    mp1.start();
                    text.setText(String.format(Locale.getDefault(), "%.2f", h));
                    text2.setText(String.format(Locale.getDefault(), "%.2f", h));

                } else {
                    mp2.start();
                    text2.setText(String.format(Locale.getDefault(), "%.2f", h));
                }

                ballInAir = false;  //Ball has reached hand
            }

        }, (int)t*2000);  //Vibrate when the ball reaches hand again
    }

    public float findHighest(float r[]) {
        float highest = 0.0f;
        for(int i = 0; i < windowSize; i++) {
            if (r[i] > highest) {
                highest = r[i];
            }
        }
        return highest;
    }

    public void initMediaPlayers() {
        mp = MediaPlayer.create(this, R.raw.pling);
        mp1 = MediaPlayer.create(this, R.raw.cheer);
        mp2 = MediaPlayer.create(this, R.raw.boo);
    }

    public void initTextViews() {
        text = findViewById(R.id.txt_bestheight);
        text1 = findViewById(R.id.txt_accelerationcurrent);
        text2 = findViewById(R.id.txt_heightlast);
    }

    public void initButtons() {
        settingsButton = findViewById(R.id.btn_settings);
    }

}
