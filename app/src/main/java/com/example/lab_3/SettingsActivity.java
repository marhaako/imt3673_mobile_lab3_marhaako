package com.example.lab_3;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    private static final int PROGRESS_MIN = 15;
    private static final int PROGRESS_MAX = 50;
    private static int minAcc;
    private Button saveButton;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initButtons();

        Intent intent = this.getIntent();
        minAcc = intent.getIntExtra("minAcc", 20);

        final SeekBar seekBar = findViewById(R.id.seekBar);
        final TextView seekBarValue = findViewById(R.id.txt_sensitivity);
        seekBarValue.setText(Integer.toString(minAcc));

        seekBar.setMin(PROGRESS_MIN);
        seekBar.setMax(PROGRESS_MAX);
        seekBar.setProgress(minAcc);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarValue.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("minAcc", seekBar.getProgress());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }

    public void initButtons() {
        saveButton = findViewById(R.id.btn_save);
    }

}
